//AAAAAAAAAAAAAAAAAAAAA

// SHIT TO DO

// - Make it so that bullets kill the player they touch
// - Change it from one player to a list of players
// - Write "addPlayer" to create another player in the game
// - 

var time=0;

var gameSpace = document.getElementById("myCanvas");
var gc = gameSpace.getContext("2d");
var space = gameSpace.getBoundingClientRect();
var buffer = 25; // space between center of player and edge of player sprite
var map = new Image();
//map.src = "./testMap.png";
//map.src = "./fuckingShittyAssMap.png"; //horizontal = cyan, vertical = magenta
map.src = "./t.png"; //yeah

//draws a simple green border around the map
function drawBorder() {
  gc.beginPath();
  gc.rect(0, 0, gameSpace.width, gameSpace.height);
  gc.lineWidth = 2;
  gc.fillStyle = 'rgba(255,255,255,255)';
  gc.fill();
  gc.strokeStyle = '#00ff00';
  gc.stroke();
  gc.closePath();
}

//draws a map from a given image
function drawMap() {
  gc.drawImage(map, 0,0,800,600);
}

//check if the area the player will move to contains a wall
function checkForWalls(x,y,buf) {
  var slice = gc.getImageData(x-buf,y-buf,2*buf,2*buf);
  var pix = slice.data;

  for(var i=0; i<pix.length; i+=4) {
      var ret;
    if(pix[i  ] === 255 &&
       pix[i+1] === 0   &&
       pix[i+2] === 255) return 1;
    
    if(pix[i  ] === 0   &&
       pix[i+1] === 255 &&
       pix[i+2] === 255) return 2;

  }
  return 0;
}

//Undo player movement when they intersect with a wall
function checkWalls(i) {
  if(checkForWalls(players[i].x, players[i].y,buffer)) {
    // console.log('AAAAAAAA');
    players[i].x -= players[i].vx;
    players[i].y -= players[i].vy;
  } 
}

//draw an object passed in
//need to give x,y location, radius, color
function drawSprite(s) {
  gc.beginPath();
  gc.fillStyle = s.c;
  gc.strokeStyle = s.c;
  gc.arc(s.x,s.y,s.r,0,2*Math.PI);
  gc.fill();
  gc.stroke();
  gc.closePath();
}

//splash screen
function splashScreen() {
  gc.beginPath();
  gc.font = "30px Comic Sans MS";
  gc.fillStyle = "blue";
  gc.textAlign = 'center';
  var str = 'clikc to play gaem';
  gc.fillText(str, gameSpace.width/2.0, gameSpace.height/2.0);
  var move = 'wasd to mov';
  gc.fillText(move, gameSpace.width/2.0, gameSpace.height/2.0+30);
  var shot = 'lef click to shoot you have infinite bullet';
  gc.fillText(shot, gameSpace.width/2.0, gameSpace.height/2.0+60);
  var shot = 'q to add new player, e to switch player';
  gc.fillText(shot, gameSpace.width/2.0, gameSpace.height/2.0+90);
  gc.styleStyle = 'blue';
  gc.closePath();
}

var moveSpeed = 2;     //player movement speed
var players = [];      //list of players in game
var currentPlayer = 0; //which player is currently selected

//test player
var player = {
  id : "fucfking ssutpid idiot nerdface",
  x : 150, // starting x
  y : 50, // starting y
  vx : 0, 
  vy : 0, 
  r : 20, //player size
  c : '#00ca44', // GREEN FLASH
};

players.push(player);

//determines the unique player id for new players entering the game
var playerid = 1;

//add a new player to the game
function addPlayer() {
  players.push({
    id: playerid,
    x:  150,
    y:  50,
    vx: 0,
    vy: 0,
    r:  20,
    c: '#15b7ed'
  });
  console.log('added player with id: '+playerid);
  playerid++;
}

function switchPlayer() {
  currentPlayer++;
  if(currentPlayer >= players.length) currentPlayer = 0;
  console.log('current player: '+currentPlayer);
}

function drawAllPlayers() {
  for(var i=0; i<players.length; i++) {
    drawSprite(players[i]);
  }
}

//Bullets n shit
var bullets = [];
var bulletSpeed = 5;
var bulletSize = 5;

//does this really need documentation
function drawBullets(arr) {
  for(b in arr) {
    drawSprite(arr[b]);
  }
}

//dot product
function dot(ax,ay,bx,by) {
  return((ax*bx)+(ay*by));
}

//magnitude of a 2D vector
function magnitude(x,y) {
  return Math.sqrt(dot(x,y,x,y));
}

//check bullet-wall intersections
function bulletWalls() {
  for(b in bullets) {
    if(checkForWalls(bullets[b].x, bullets[b].y, bulletSize*2) === 1) {
      bullets[b].vy *= -1;
      bullets[b].life -= 1;
    }
    if(checkForWalls(bullets[b].x, bullets[b].y, bulletSize*2) === 2) {
      //vertical
      bullets[b].vx *= -1;
      bullets[b].life -= 1;
    }
    if(checkForWalls(bullets[b].x, bullets[b].y, bulletSize*2) === 3) {
      bullets[b].vx *= -1;
      bullets[b].vy *= -1;
      bullets[b].life -= 1;
    }
  }
}

//remove bullets whose lifespans have expired
function removeDeadBullets() {
  for(b in bullets) {
  //  console.log(bullets[b]);
    if(bullets[b].life < 0) bullets.splice(b,1);
  }
}

//generate a new bullet on click
function shoot(e,i) {
  //time=0;
  var rect = gameSpace.getBoundingClientRect();
  mouseX = e.clientX - rect.left;
  mouseY = e.clientY - rect.top;
  var dx = mouseX - players[i].x;
  var dy = mouseY - players[i].y;
  var mag = Math.sqrt((dx*dx) + (dy*dy));
  var bvx = bulletSpeed * (dx / mag);
  var bvy = bulletSpeed * (dy / mag);
  var shot = {
    x : players[i].x, //initial x position
    y : players[i].y, //initial y position
    vx : bvx,
    vy : bvy,
    r : bulletSize,
    c : '#ff0000',
    t : time,
    life : 10
  };
  //console.log("bullet shot!");
  bullets.push(shot);
  //console.log(shot);
}

//advance bullets every frame
function bulletStep() {
  for(b in bullets) {
    bullets[b].x += bullets[b].vx;
    bullets[b].y += bullets[b].vy;
  }
}

function updateBulletAndInsert(b) {
  //console.log("bullet time to update " + time-b.t);
  for(var i = 0; i < time-b.t; ++i) {
    //console.log("foo");
    b.x += b.vx;
    b.y += b.vy;
    if(checkForWalls(b.x, b.y, 2*bulletSize) === 1) {
      //vertical
      b.vy *= -1;
      b.life -= 1;
    }
    if(checkForWalls(b.x, b.y, 2*bulletSize) === 2) {
      //horizontal
      b.vx *= -1;
      b.life -= 1;
    }
    if(checkForWalls(b.x, b.y, 2*bulletSize) === 3) {
      b.vx *= -1;
      b.vy *= -1;
      b.life -= 1;
    }
  }
  bullets.push(b);
}

var started = false;
gameSpace.onmousedown = function() { started = true; }

//advance player - called on keypress
function movePlayer(i) {
  players[i].x += players[i].vx;
  players[i].y += players[i].vy;
}

// check if the player is trying to leave the frame
function checkBounds(i) {
  if(players[i].x < buffer) players[i].x -= players[i].vx;
  if(players[i].y < buffer) players[i].y -= players[i].vy;
  if(players[i].x > gameSpace.width - buffer) players[i].x -= players[i].vx;
  if(players[i].y > gameSpace.height - buffer) players[i].y -= players[i].vy;
}

var keyPressed = {'w':false, 'a':false, 's':false, 'd':false, 'e':false, 'q':false};

//add/switch player on key press
function addswitch(e) {
  var keycode = e.keycode;
  if(keycode === 81) {
    keyPressed.q = true;
    addPlayer();
  } else if(keycode === 69) {
    keyPressed.e = true;
    switchPlayer();
  }
}

//move player based on key press
function updateVels(e, i) {
  var keycode = e.keyCode;
  if(keycode === 87) {
    //w
    keyPressed.w = true;
    players[i].vy = -moveSpeed;
  } else if(keycode === 65) {
    //a
    keyPressed.a = true;
    players[i].vx = -moveSpeed;
  } else if(keycode === 83) {
    //s
    keyPressed.s = true;
    players[i].vy = moveSpeed;
  } else if(keycode === 68) {
    //d
    keyPressed.d = true;
    players[i].vx = moveSpeed;
  }
  else if(keycode === 81) { 
    keyPressed.q = true;
    addPlayer();
    //playerUpdateBulletListEvent(); //hijacked to test my own thing
  } else if(keycode === 69) {
    keyPressed.e = true;
    switchPlayer();
  }
}

//stop adding velocity once the key stops being pressed
function resetVels(e,i) {
  var keycode = e.keyCode;
  if(keycode === 87 || keycode === 83) players[i].vy = 0;
  if(keycode === 65 || keycode === 68) players[i].vx = 0;
}

//-----------------------------------------------SQL QUERY SPECIFIC STUFF------------------------------------------------

function playerAddBulletEvent()
{
    //call insert Bullet Ajax Call on new bullet
    //send ajax update event to other players
    //var conn = new XMLHttpRequest();
    //conn.open("GET", "

}

function parseThatShit(s) {
  var lines = s.split(" ");
  var newbullets = [];
  for(i in lines) {
    if(lines[i].length > 1) {
      var comp = lines[i].split(",");
      newbullets.push({
        x:    comp[0],
        y:    comp[1],
        vy:   comp[2],
        vx:   comp[3],
        r:    bulletSize,
        c:    '#ff0000',
        t:    comp[4],
        life: comp[5]
      });
    }
  }
  return newbullets;
}

function playerUpdateBulletListEvent()
{
    //make call to sql, get new bullets
    var conn = new XMLHttpRequest();
    var result;
    conn.onreadystatechange = function() {
      if(conn.readyState == 4) {
        if(conn.status == 200) {
          result = conn.responseText;
          //console.log(result);
         
          var arr = [];
          //do the parsing of result here
          arr = parseThatShit(result);

          console.log('arr: ');
          for(i in arr) console.log(arr[i]);

          for(bullet in arr)
          {
             //console.log(time);
            if(time > arr[bullet].t)
            {
                updateBulletAndInsert(arr[bullet]);
            }
            else
            {
              bullets.push(arr[bullet]);
            }
          }
        }
      }
    }
    console.log('bullets: ');
          for(i in bullets) console.log(bullets[i]);
   conn.open("GET", "getbullet.php", true);
   conn.send(); 
}

//get rid of the bullet and some other stuff that I forgot
function redoBulletList() {
  
}

//insert a single bullet into the table
function insertBullet() {
  
}

//-----------------------------------------------------------------------------------------------------------------------




//stuff that happens every frame
var gameloop = setInterval(function() {
  if(!started) splashScreen();
  else {
    gameSpace.onmousedown = function(e) {shoot(e,currentPlayer);}
    document.onkeydown = function(e) {
      updateVels(e,currentPlayer);
    }
    document.onkeyup = function(e) {resetVels(e,currentPlayer);}
    drawBorder();
    movePlayer(currentPlayer);
    drawMap();
    checkBounds(currentPlayer);
    checkWalls(currentPlayer);
    playerUpdateBulletListEvent();
    drawBullets(bullets);
    bulletStep(bullets);
    bulletWalls();
    drawBullets(bullets);
    removeDeadBullets();
    drawSprite(players[currentPlayer]);
    drawAllPlayers();
    time++;
  }
}, 12);
