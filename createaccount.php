<?php
  if($_SERVER['SERVER_NAME'] != "dias12.cs.trinity.edu") {
    echo"<p>You must access this page from on campus through dias12.</p></body></html>";
    die();
  }
  session_start();
?>
<html>
<head>
  <title>Create Account</title>
  <link rel="stylesheet" type="text/css" href="../Styles/newstyle.css">
  <ul>
    <li><a href="http://cs.trinity.edu/~zzimdars/WebApps">Home</a></li>
    <li><a href='../AboutMe.html'>About Me</a></li>
    <li><a href='../Tasks.html'>Tasks</a></li>
    <li><a href='../Spooky.html'>Prepare to Get Spooked</a></li>
  </ul>
</head>
<body>
  <br><br><br><br><br><br><br>

  <form method="POST">
    Enter a Username: <input type="text" name="newuser"><br>
    Enter a Password: <input type="password" name="newpass"><br>
    <br><br><br>
    <input type="Submit" value="Create Account">
  </form>

<?php
  $_SESSION["newuser"] = $_POST["newuser"];
  $_SESSION["newpass"] = $_POST["newpass"];

  if($_SESSION["newuser"] && $_SESSION["newpass"]) {
    header('Location: newaccount.php');

  }
?>
