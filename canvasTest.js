//AAAAAAAAAAAAAAAAAAAAA
var gameSpace = document.getElementById("myCanvas");
var gc = gameSpace.getContext("2d");
var space = gameSpace.getBoundingClientRect();
var buffer = 25; // space between center of player and edge of player sprite
var map = new Image();
//map.src = "./testMap.png";
map.src = "./fuckingShittyAssMap.png";


function drawBorder() {
  gc.beginPath();
  gc.rect(0, 0, gameSpace.width, gameSpace.height);
  gc.lineWidth = 2;
  gc.fillStyle = 'white';
  gc.fill();
  gc.strokeStyle = '#00ff00';
  gc.stroke();
  gc.closePath();
}

function drawMap() {
  gc.drawImage(map, 0,0,800,600);
}

function drawWalls() {
  gc.beginPath();
  gc.lineWidth = 5;
  gc.moveTo(100,100);
  gc.lineTo(400,100);
  gc.strokeStyle = '#ff00ff';
  gc.stroke();
  gc.closePath();
}

function checkForWalls(x,y,buf) {
  //check if the area the player will move to contains a wall
  //(x,y) is the center of the player at the new location
  var slice = gc.getImageData(x-buf,y-buf,2*buf,2*buf);
  var pix = slice.data;

  for(var i=0; i<pix.length; i+=4) {
      var ret;
    if(pix[i  ] === 255 &&
       pix[i+1] === 0   &&
       pix[i+2] === 255) return 1;
    
    if(pix[i  ] === 0   &&
       pix[i+1] === 255 &&
       pix[i+2] === 255) return 2;

  }
  return 0;
}

function checkWalls() {
  if(checkForWalls(player.x, player.y,buffer)) {
    // console.log('AAAAAAAA');
    player.x -= player.vx;
    player.y -= player.vy;
  } 
}

function drawSprite(s) {
  gc.beginPath();
  gc.fillStyle = s.c;
  gc.strokeStyle = s.c;
  gc.arc(s.x,s.y,s.r,0,2*Math.PI);
  gc.fill();
  gc.stroke();
  gc.closePath();
}

//splash screen maybe?
function splashScreen() {
  gc.beginPath();
  gc.font = "30px Comic Sans MS";
  gc.fillStyle = "blue";
  gc.textAlign = 'center';
  var str = 'clikc to play gaem';
  gc.fillText(str, gameSpace.width/2.0, gameSpace.height/2.0);
  var move = 'wasd to mov';
  gc.fillText(move, gameSpace.width/2.0, gameSpace.height/2.0+30);
  var shot = 'lef click to shoot you have infinite bullet';
  gc.fillText(shot, gameSpace.width/2.0, gameSpace.height/2.0+60);
  gc.styleStyle = 'blue';
  gc.closePath();
}

var moveSpeed = 2;
var player = {
  id : "fucfking ssutpid idiot nerdface",
  x : 50, // starting x
  y : 50, // starting y
  vx : 0, 
  vy : 0, 
  r : 20, //player size
  c : '#00ca44' // GREEN FLASH
};

// Bullets n shit
var bullets = [];
var bulletSpeed = 5;
var bulletSize = 5;

function drawBullets(arr) {
  for(b in arr) {
    drawSprite(arr[b]);
  }
}

function dot(ax,ay,bx,by) {
  return((ax*bx)+(ay*by));
}

function magnitude(x,y) {
  return Math.sqrt(dot(x,y,x,y));
}

function bulletWalls() {
  for(b in bullets) {
    if(checkForWalls(bullets[b].x, bullets[b].y, bulletSize*2) === 1) {
      //horizontal wall
      //bullets.splice(b,1);
      /*
      var theta = Math.PI - Math.acos(dot(bullets[b].vx, bullets[b].vy, 0, 1)/magnitude(bullets[b].vx,bullets[b].vy));
      bullets[b].vx = Math.cos(theta) * bulletSpeed;
      bullets[b].vy = Math.sin(theta) * bulletSpeed;
      */
      bullets[b].vy *= -1;
      bullets[b].life -= 1;
    }
    if(checkForWalls(bullets[b].x, bullets[b].y, bulletSize*2) === 2) {
      //vertical
      //bullets.splice(b,1);
      bullets[b].vx *= -1;
      bullets[b].life -= 1;
    }
    if(checkForWalls(bullets[b].x, bullets[b].y, bulletSize*2) === 3) {
      bullets[b].vx *= -1;
      bullets[b].vy *= -1;
      bullets[b].life -= 1;
    }
  }
}

function removeDeadBullets() {
  for(b in bullets) {
    if(bullets[b].life < 0) bullets.splice(b,1);
  }
}

function shoot(e) {
  var rect = gameSpace.getBoundingClientRect();
  mouseX = e.clientX - rect.left;
  mouseY = e.clientY - rect.top;
  var dx = mouseX - player.x;
  var dy = mouseY - player.y;
  var mag = Math.sqrt((dx*dx) + (dy*dy));
  var bvx = bulletSpeed * (dx / mag);
  var bvy = bulletSpeed * (dy / mag);
  var shot = {
    x : player.x,
    y : player.y,
    vx : bvx,
    vy : bvy,
    r : bulletSize,
    c : '#ff0000',
    life : 5
  };
  bullets.push(shot);
}

function bulletStep() {
  for(b in bullets) {
    bullets[b].x += bullets[b].vx;
    bullets[b].y += bullets[b].vy;
  }
}

var started = false;
gameSpace.onmousedown = function() { started = true; }

function movePlayer() {
  player.x += player.vx;
  player.y += player.vy;
}

function checkBounds() {
  // check if the player is trying to leave the frame
  if(player.x < buffer) player.x -= player.vx;
  if(player.y < buffer) player.y -= player.vy;
  if(player.x > gameSpace.width - buffer) player.x -= player.vx;
  if(player.y > gameSpace.height - buffer) player.y -= player.vy;
}

var keyPressed = {'w':false, 'a':false, 's':false, 'd':false};

function updateVels(e) {
  var keycode = e.keyCode;
  if(keycode === 87) {
    //w
    keyPressed.w = true;
    player.vy = -moveSpeed;
  } else if(keycode === 65) {
    //a
    keyPressed.a = true;
    player.vx = -moveSpeed;
  } else if(keycode === 83) {
    //s
    keyPressed.s = true;
    player.vy = moveSpeed;
  } else if(keycode === 68) {
    //d
    keyPressed.d = true;
    player.vx = moveSpeed;
  }
}

function resetVels(e) {
  var keycode = e.keyCode;
  if(keycode === 87 || keycode === 83) player.vy = 0;
  if(keycode === 65 || keycode === 68) player.vx = 0;
}

var gameloop = setInterval(function() {
  if(!started) splashScreen();
  else {
    gameSpace.onmousedown = function(e) {shoot(e);}
    document.onkeydown = function(e) {updateVels(e);}
    document.onkeyup = function(e) {resetVels(e);}
    drawBorder();
    movePlayer();
    //drawWalls();
    drawMap();
    checkBounds();
    checkWalls();
    bulletStep(bullets);
    bulletWalls();
    drawBullets(bullets);
    removeDeadBullets();
    drawSprite(player);
  }
}, 12);
