s a structural feature mainly used in automobiles and recently incorporated into
railcars.[1][2][3][4][5][6]

Crumple zones are designed to absorb the energy from the impact during a traffic collision by
controlled deformation. This energy is much greater than is commonly realized. A 2,000 kg (4,409 lb)
car travelling at 60 km/h (37 mph) (16.7 m/s), before crashing into a thick concrete wall, is
subject to the same impact force as a front-down drop from a height of 14.2 m (47 ft) crashing on to
a solid concrete surface.[7] Increasing that speed by 50% to 90 km/h (56 mph) (25 m/s) compares to a
fall from 32 m (105 ft) - an increase of 125%. [7] This is because the stored kinetic energy (E) is
given by E = (1/2) mass × speed squared. It increases by the square of the impact velocity.[7][8]

Typically, crumple zones are located in the front part of the vehicle, in order to absorb the impact
of a head-on collision, though they may be found on other parts of the vehicle as well. According to
a British Motor Insurance Repair Research Centre study of where on the vehicle impact damage occurs:
65% were front impacts, 25% rear impacts, 5% left side, and 5% right side.[9] Some racing cars use
aluminium, composite/carbon fibre honeycomb, or energy absorbing foam[10][11] to form an impact
attenuator that dissipates crash energy using a much smaller volume and lower weight than road car
crumple zones.[12] Impact attenuators have also been introduced on highway maintenance vehicles in
some countries.

An early example of the crumple zone concept was used by the Mercedes-Benz engineer Béla Barényi on
the mid 1950s Mercedes-Benz "Ponton".[13] This innovation was first patented by Mercedes-Benz in the
early 1950s. The patent 854157, granted in 1952, describes the decisive feature of passive safety.
Barényi questioned the opinion prevailing until then, that a safe car had to be rigid. He divided
the car body into three sections: the rigid non-deforming passenger compartment and the crumple
zones in the front and the rear. They are designed to absorb the energy of an impact (kinetic
energy) by deformation during collision.[14]

On September 10, 2009, the ABC News programs Good Morning America and World News showed a U.S.
Insurance Institute for Highway Safety crash test of a 2009 Chevrolet Malibu in an offset head-on
collision with a 1959 Chevrolet Bel Air sedan. It dramatically demonstrated the effectiveness of
modern car safety design over 1950s design, particularly of rigid passenger safety cells and crumple
zones.[15] [16]
