//AAAAAAAAAAAAAAAAAAAAA

// SHIT TO DO
//
// - Make it so that bullets kill the player they touch
// - Change it from one player to a list of players
// - Write "addPlayer" to create another player in the game

var gameSpace = document.getElementById("myCanvas");
var gc = gameSpace.getContext("2d");
var space = gameSpace.getBoundingClientRect();
var buffer = 25; // space between center of player and edge of player sprite
var map = new Image();
//map.src = "./testMap.png";
map.src = "./fuckingShittyAssMap.png"; //horizontal = cyan, vertical = magenta

//draws a simple green border around the map
function drawBorder() {
  gc.beginPath();
  gc.rect(0, 0, gameSpace.width, gameSpace.height);
  gc.lineWidth = 2;
  gc.fillStyle = 'rgba(255,255,255,255)';
  gc.fill();
  gc.strokeStyle = '#00ff00';
  gc.stroke();
  gc.closePath();
}

//draws a map from a given image
function drawMap() {
  gc.drawImage(map, 0,0,800,600);
}

//UNUSED - draws a test wall
function drawWalls() {
  gc.beginPath();
  gc.lineWidth = 5;
  gc.moveTo(100,100);
  gc.lineTo(400,100);
  gc.strokeStyle = '#ff00ff';
  gc.stroke();
  gc.closePath();
}

//check if the area the player will move to contains a wall
function checkForWalls(x,y,buf) {
  //(x,y) is the player location, buf is the size of the player's hitbox
  var slice = gc.getImageData(x-buf,y-buf,2*buf,2*buf);
  var pix = slice.data;

  for(var i=0; i<pix.length; i+=4) {
      var ret;
    if(pix[i  ] === 255 &&
       pix[i+1] === 0   &&
       pix[i+2] === 255) return 1;
    
    if(pix[i  ] === 0   &&
       pix[i+1] === 255 &&
       pix[i+2] === 255) return 2;

  }
  return 0;
}

//Undo player movement when they intersect with a wall
function checkWalls() {
  if(checkForWalls(player.x, player.y,buffer)) {
    // console.log('AAAAAAAA');
    player.x -= player.vx;
    player.y -= player.vy;
  } 
}

//draw an object passed in
//need to give x,y location, radius, color
function drawSprite(s) {
  gc.beginPath();
  gc.fillStyle = s.c;
  gc.strokeStyle = s.c;
  gc.arc(s.x,s.y,s.r,0,2*Math.PI);
  gc.fill();
  gc.stroke();
  gc.closePath();
}

//splash screen
function splashScreen() {
  gc.beginPath();
  gc.font = "30px Comic Sans MS";
  gc.fillStyle = "blue";
  gc.textAlign = 'center';
  var str = 'clikc to play gaem';
  gc.fillText(str, gameSpace.width/2.0, gameSpace.height/2.0);
  var move = 'wasd to mov';
  gc.fillText(move, gameSpace.width/2.0, gameSpace.height/2.0+30);
  var shot = 'lef click to shoot you have infinite bullet';
  gc.fillText(shot, gameSpace.width/2.0, gameSpace.height/2.0+60);
  gc.styleStyle = 'blue';
  gc.closePath();
}

//player movement speed
var moveSpeed = 2;
var player = {
  id : "fucfking ssutpid idiot nerdface",
  x : 50, // starting x
  y : 50, // starting y
  vx : 0, 
  vy : 0, 
  r : 20, //player size
  c : '#00ca44', // GREEN FLASH
  bullets : []
};

//Bullets n shit
var bulletSpeed = 5;
var bulletSize = 5;

//does this really need documentation
function drawBullets(arr) {
  for(b in arr) {
    drawSprite(arr[b]);
  }
}

//dot product
function dot(ax,ay,bx,by) {
  return((ax*bx)+(ay*by));
}

//magnitude of a 2D vector
function magnitude(x,y) {
  return Math.sqrt(dot(x,y,x,y));
}

//check bullet-wall intersections
function bulletWalls() {
  for(b in player.bullets) {
    if(checkForWalls(player.bullets[b].x, player.bullets[b].y, bulletSize*2) === 1) {
      player.bullets[b].vy *= -1;
      player.bullets[b].life -= 1;
    }
    if(checkForWalls(player.bullets[b].x, player.bullets[b].y, bulletSize*2) === 2) {
      //vertical
      player.bullets[b].vx *= -1;
      player.bullets[b].life -= 1;
    }
    if(checkForWalls(player.bullets[b].x, player.bullets[b].y, bulletSize*2) === 3) {
      player.bullets[b].vx *= -1;
      player.bullets[b].vy *= -1;
      player.bullets[b].life -= 1;
    }
  }
}

//remove bullets whose lifespans have expired
function removeDeadBullets() {
  for(b in player.bullets) {
    if(player.bullets[b].life < 0) player.bullets.splice(b,1);
  }
}

//generate a new bullet on click
function shoot(e) {
  var rect = gameSpace.getBoundingClientRect();
  mouseX = e.clientX - rect.left;
  mouseY = e.clientY - rect.top;
  var dx = mouseX - player.x;
  var dy = mouseY - player.y;
  var mag = Math.sqrt((dx*dx) + (dy*dy));
  var bvx = bulletSpeed * (dx / mag);
  var bvy = bulletSpeed * (dy / mag);
  var shot = {
    x : player.x,
    y : player.y,
    vx : bvx,
    vy : bvy,
    r : bulletSize,
    c : '#ff0000',
    life : 10
  };
  player.bullets.push(shot);
}

//advance bullets every frame
function bulletStep() {
  for(b in player.bullets) {
    player.bullets[b].x += player.bullets[b].vx;
    player.bullets[b].y += player.bullets[b].vy;
  }
}

var started = false;
gameSpace.onmousedown = function() { started = true; }

//advance player - called on keypress
function movePlayer() {
  player.x += player.vx;
  player.y += player.vy;
}

// check if the player is trying to leave the frame
function checkBounds() {
  if(player.x < buffer) player.x -= player.vx;
  if(player.y < buffer) player.y -= player.vy;
  if(player.x > gameSpace.width - buffer) player.x -= player.vx;
  if(player.y > gameSpace.height - buffer) player.y -= player.vy;
}

var keyPressed = {'w':false, 'a':false, 's':false, 'd':false};

//move player based on key press
function updateVels(e) {
  var keycode = e.keyCode;
  if(keycode === 87) {
    //w
    keyPressed.w = true;
    player.vy = -moveSpeed;
  } else if(keycode === 65) {
    //a
    keyPressed.a = true;
    player.vx = -moveSpeed;
  } else if(keycode === 83) {
    //s
    keyPressed.s = true;
    player.vy = moveSpeed;
  } else if(keycode === 68) {
    //d
    keyPressed.d = true;
    player.vx = moveSpeed;
  }
}

//stop adding velocity once the key stops being pressed
function resetVels(e) {
  var keycode = e.keyCode;
  if(keycode === 87 || keycode === 83) player.vy = 0;
  if(keycode === 65 || keycode === 68) player.vx = 0;
}

//stuff that happens every frame
var gameloop = setInterval(function() {
  if(!started) splashScreen();
  else {
    gameSpace.onmousedown = function(e) {shoot(e);}
    document.onkeydown = function(e) {updateVels(e);}
    document.onkeyup = function(e) {resetVels(e);}
    drawBorder();
    movePlayer();
    //drawWalls();
    drawMap();
    checkBounds();
    checkWalls();
    bulletStep(player.bullets);
    bulletWalls();
    drawBullets(player.bullets);
    removeDeadBullets();
    drawSprite(player);
  }
}, 12);
